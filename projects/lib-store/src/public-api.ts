/*
 * Public API
 */

export { LibStoreModule } from './lib/lib-store.module';

export { TopicsStore } from './lib/stores/topics-store.service';
export { TagsStore } from './lib/stores/tags-store.service';

export { ResourcesService } from './lib/services/resources.service';
export { TagsService } from './lib/services/tag.service';
export { TopicsService } from './lib/services/topics.service';
