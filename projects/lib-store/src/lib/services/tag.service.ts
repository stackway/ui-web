import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { Tag } from '@infrastructure/rest';

import { TagsStore } from '../stores/tags-store.service';

@Injectable({ providedIn: 'root' })
export class TagsService {
  constructor(private readonly tagsStore: TagsStore) {}

  public addTag(tag: Tag): Observable<boolean> {
    return this.tagsStore.addTag(tag).pipe(
      map(addedTag => {
        if (!addedTag) {
          return false;
        }

        return true;
      })
    );
  }

  public removeTag(tag: Tag): Observable<boolean> {
    return this.tagsStore.removeTag(tag).pipe(
      map(removedTag => {
        if (!removedTag) {
          return false;
        }

        this.tagsStore.onRemoveTag.next(tag);

        return true;
      })
    );
  }

  public hideTag(tag: Tag): Observable<boolean> {
    tag.isHidden = true;

    return this.tagsStore
      .updateTag(tag)
      .pipe(map(updatedTag => (updatedTag ? true : false)));
  }
}
