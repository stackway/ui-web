import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { TopicsEndpoint } from '@infrastructure/rest';
import { UserManager } from '@lib/auth';

@Injectable({ providedIn: 'root' })
export class TopicsService {
  constructor(
    private readonly endpoint: TopicsEndpoint,
    private readonly userManager: UserManager
  ) {}

  public forceUpdate(): Observable<boolean> {
    const user = this.userManager.getUser();

    if (!user) {
      return of(false);
    }

    return this.endpoint.forceUpdateResources(user.id);
  }
}
