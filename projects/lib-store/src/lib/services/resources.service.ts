import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthService } from '@lib/auth';

import { ResourcesEndpoint, Topic, Resource } from '@infrastructure/rest';

import { TopicsStore } from '../stores/topics-store.service';

@Injectable({ providedIn: 'root' })
export class ResourcesService {
  constructor(
    private readonly endpoint: ResourcesEndpoint,
    private readonly authService: AuthService,
    private readonly topicsStore: TopicsStore
  ) {}

  public archive(topic: Topic, resource: Resource): Observable<Resource | null> {
    const token = this.authService.getToken();

    if (token === null) {
      return of(null);
    }

    return this.endpoint
      .useJwt(token)
      .archive(topic.id, resource.id)
      .pipe(
        map(isSuccess => {
          if (!isSuccess) {
            return null;
          }

          topic.actualResources = topic.actualResources.filter(r => r.id !== resource.id);

          this.topicsStore.onTopicUpdate.next(topic);

          return resource;
        })
      );
  }
}
