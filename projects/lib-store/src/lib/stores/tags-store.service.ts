import { Injectable } from '@angular/core';

import { Observable, of, Subject, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { TagsEndpoint, Tag } from '@infrastructure/rest';

import { AuthService, UserManager } from '@lib/auth';

@Injectable({ providedIn: 'root' })
export class TagsStore {
  constructor(
    private readonly endpoint: TagsEndpoint,
    private readonly authService: AuthService,
    private readonly userManager: UserManager
  ) {}

  public onTagsPull: Subject<Tag[]> = new BehaviorSubject([]);
  public onTagPull: Subject<Tag | null> = new Subject();

  public onAddTag: Subject<Tag | null> = new Subject();
  public onUpdateTag: Subject<Tag | null> = new Subject();
  public onRemoveTag: Subject<Tag | null> = new Subject();

  public addTag(tag: Tag): Observable<Tag | null> {
    const token = this.authService.getToken();

    if (token === null) {
      return of(null);
    }

    const user = this.userManager.getUser();

    if (user === null) {
      return of(null);
    }

    return this.endpoint
      .useJwt(token)
      .addTag(user.id, tag)
      .pipe(
        map(isSuccess => {
          if (isSuccess) {
            this.onAddTag.next(tag);

            return tag;
          }

          return null;
        })
      );
  }

  public getTags(): Observable<Tag[]> {
    const token = this.authService.getToken();

    if (token === null) {
      return of([]);
    }

    const user = this.userManager.getUser();

    if (user === null) {
      return of([]);
    }

    return this.endpoint
      .useJwt(token)
      .getTags(user.id)
      .pipe(
        map(tags => {
          if (tags.length > 0) {
            this.onTagsPull.next(tags);
          }

          return tags;
        })
      );
  }

  public getTag(tagId: number): Observable<Tag | null> {
    const token = this.authService.getToken();

    if (token === null) {
      return of(null);
    }

    const user = this.userManager.getUser();

    if (user === null) {
      return of(null);
    }

    return this.endpoint
      .useJwt(token)
      .getTag(user.id, tagId)
      .pipe(
        map(tag => {
          if (tag !== null) {
            this.onTagPull.next(tag);
          }

          return tag;
        })
      );
  }

  public updateTag(tag: Tag): Observable<Tag | null> {
    const token = this.authService.getToken();

    if (token === null) {
      return of(null);
    }

    return this.endpoint
      .useJwt(token)
      .updateTag(tag)
      .pipe(
        map(isSuccess => {
          if (isSuccess) {
            this.onUpdateTag.next(tag);

            return tag;
          }

          return null;
        })
      );
  }

  public removeTag(tag: Tag): Observable<Tag | null> {
    const token = this.authService.getToken();

    if (token === null) {
      return of(null);
    }

    const user = this.userManager.getUser();

    if (user === null) {
      return of(null);
    }

    return this.endpoint
      .useJwt(token)
      .removeTag(user.id, tag)
      .pipe(
        map(isSuccess => {
          if (isSuccess) {
            this.onRemoveTag.next(tag);

            return tag;
          }

          return null;
        })
      );
  }
}
