import { Injectable } from '@angular/core';

import { Observable, Subject, of, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { TopicsEndpoint, Topic } from '@infrastructure/rest';

import { AuthService, UserManager } from '@lib/auth';

@Injectable({
  providedIn: 'root'
})
export class TopicsStore {
  constructor(
    private readonly endpoint: TopicsEndpoint,
    private readonly authService: AuthService,
    private readonly userManager: UserManager
  ) {}

  public onTopicsPull: Subject<Topic[]> = new BehaviorSubject([]);
  public onTopicPull: Subject<Topic | null> = new Subject();

  public onTopicUpdate: Subject<Topic | null> = new Subject();

  public getTopics(): Observable<Topic[]> {
    const token = this.authService.getToken();

    if (token === null) {
      return of([]);
    }

    const user = this.userManager.getUser();

    if (user === null) {
      return of([]);
    }

    return this.endpoint
      .useJwt(token)
      .getTopics(user.id)
      .pipe(
        map(topics => {
          this.onTopicsPull.next(topics);

          return topics;
        })
      );
  }

  public getTopic(topicId: number): Observable<Topic | null> {
    const token = this.authService.getToken();

    if (token === null) {
      return of(null);
    }

    const user = this.userManager.getUser();

    if (user === null) {
      return of(null);
    }

    return this.endpoint
      .useJwt(token)
      .getTopic(user.id, topicId)
      .pipe(
        map(topic => {
          this.onTopicPull.next(topic);

          return topic;
        })
      );
  }
}
