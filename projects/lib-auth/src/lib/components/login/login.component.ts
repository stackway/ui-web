import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { NzNotificationService } from 'ng-zorro-antd';

import { AuthService } from '../../services/auth-service';
import { UserManager } from '../../services/user-manager.service';
import { LoginRouting } from './login-routing';

@Component({
  selector: 'lib-auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public validateForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly userManager: UserManager,
    private readonly router: Router,
    private readonly notification: NzNotificationService,
    private readonly builder: FormBuilder,
    private readonly options: LoginRouting
  ) {}

  public ngOnInit(): void {
    this.authService.isAuthenticated().subscribe(isSuccess => {
      if (isSuccess && this.options.success) {
        this.router.navigate([this.options.success]);
      }
    });

    this.validateForm = this.builder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(8)]],
      remember: [true]
    });
  }

  public onLogin() {
    if (this.authService.getToken) {
      this.authService.logout();
    }

    const model = {
      email: this.validateForm.get('email').value,
      password: this.validateForm.get('password').value
    };

    this.authService.login(model.email, model.password).subscribe(isSuccess => {
      if (!isSuccess) {
        return this.notification.error(
          'Authentication',
          'The password or email does not match. Maybe user is not exist in the system'
        );
      }

      this.userManager.persistUser(model.email).subscribe(user => {
        if (this.options.success) {
          this.router.navigate([this.options.success]);
        }
      });
    });
  }
}
