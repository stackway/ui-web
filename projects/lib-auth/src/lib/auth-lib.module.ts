import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { NgxWebstorageModule } from 'ngx-webstorage';

import { NgZorroAntdModule, en_US, NZ_I18N } from 'ng-zorro-antd';

import { AuthGuardOptions } from './auth-guard-options';
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    NgxWebstorageModule.forRoot(),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule
  ],
  exports: [LoginComponent],
  providers: [{ provide: NZ_I18N, useValue: en_US }]
})
export class AuthLibModule {
  static forRoot(options: AuthGuardOptions): ModuleWithProviders {
    return {
      ngModule: AuthLibModule,
      providers: [{ provide: AuthGuardOptions, useValue: options }]
    };
  }
}
