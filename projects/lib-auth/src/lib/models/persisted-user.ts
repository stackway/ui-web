import { PersistedUserContract } from './persisted-user.contract';

export class PersistedUser implements PersistedUserContract {
  public id: number | null = null;
  public username: string | null = null;

  constructor(public readonly email: string) {}
}
