export interface PersistedUserContract {
  id: number | null;
  username: string | null;

  email: string;
}
