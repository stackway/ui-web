export abstract class AuthGuardOptions {
  routes: {
    invalid: Route;
    success: Route;
  };
}

export abstract class Route {
  use: boolean;
  link: string;
}
