import { Injectable } from '@angular/core';

import { Observable, of, Subject, BehaviorSubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { LocalStorageService } from 'ngx-webstorage';

import { AuthEndpoint } from '@infrastructure/rest';
import { HttpErrorResponse } from '@angular/common/http';

enum AuthStorageTokens {
  TOKEN = 'token'
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private readonly localStorage: LocalStorageService,
    private readonly endpoint: AuthEndpoint
  ) {
    this.onAuthentication = new BehaviorSubject(false);

    this.isAuthenticated().subscribe(isAuthenticated =>
      this.onAuthentication.next(isAuthenticated)
    );
  }

  public onAuthentication: Subject<boolean>;

  public isAuthenticated(): Observable<boolean> {
    const token = this.getToken();

    if (token === null) {
      this.onAuthentication.next(false);

      return of(false);
    }

    return this.endpoint.isValidToken(token).pipe(
      map(isSuccess => {
        this.onAuthentication.next(isSuccess);

        return isSuccess;
      })
    );
  }

  public login(email: string, password: string): Observable<boolean> {
    return this.endpoint.getToken(email, password).pipe(
      catchError((error: HttpErrorResponse) => of(null)),
      map((token: string | null) => {
        if (token === null) {
          this.onAuthentication.next(false);

          return false;
        }

        this.localStorage.store(AuthStorageTokens.TOKEN, token);

        this.onAuthentication.next(true);

        return true;
      })
    );
  }

  public logout() {
    this.onAuthentication.next(false);

    this.localStorage.clear(AuthStorageTokens.TOKEN);
  }

  public getToken(): string | null {
    return this.localStorage.retrieve(AuthStorageTokens.TOKEN);
  }
}
