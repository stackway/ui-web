import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { LocalStorageService } from 'ngx-webstorage';

import { UsersEndpoint, User } from '@infrastructure/rest';

import { AuthService } from './auth-service';
import { PersistedUserContract } from '../models/persisted-user.contract';

enum UserStorageTokens {
  TOKEN = 'user'
}

@Injectable({
  providedIn: 'root'
})
export class UserManager {
  constructor(
    private readonly localStorage: LocalStorageService,
    private readonly endpoint: UsersEndpoint,
    private readonly authService: AuthService
  ) {
    this.onPersistedUserChange = new BehaviorSubject(this.getUser());
  }

  public onPersistedUserChange: Subject<PersistedUserContract | null>;

  public persistUserByToken(
    email: string,
    token: string
  ): Observable<PersistedUserContract | null> {
    if (token === null) {
      return of(null);
    }

    return this.endpoint
      .useJwt(token)
      .getUserByEmail(email)
      .pipe(
        catchError((error: HttpErrorResponse) => of(null)),
        map((user: User) => {
          if (user === null) {
            return null;
          }

          return this.directlyPersistUser({
            email,
            id: user.id,
            username: user.username
          });
        })
      );
  }

  public persistUser(email: string): Observable<PersistedUserContract | null> {
    const token = this.authService.getToken();

    if (token === null) {
      return of(null);
    }

    return this.endpoint
      .useJwt(token)
      .getUserByEmail(email)
      .pipe(
        catchError((error: HttpErrorResponse) => of(null)),
        map((user: User) => {
          if (user === null) {
            return null;
          }

          return this.directlyPersistUser({
            email,
            id: user.id,
            username: user.username
          });
        })
      );
  }

  private directlyPersistUser(user: PersistedUserContract): PersistedUserContract {
    this.localStorage.store(UserStorageTokens.TOKEN, user);

    this.onPersistedUserChange.next(user);

    return user;
  }

  public clearUser() {
    this.onPersistedUserChange.next(null);

    this.localStorage.clear(UserStorageTokens.TOKEN);
  }

  public getUser(): PersistedUserContract | null {
    const contract = this.localStorage.retrieve(UserStorageTokens.TOKEN);

    if (!contract) {
      return null;
    }

    return contract;
  }

  public removeUser(): Observable<boolean> {
    const user = this.getUser();

    if (!user) {
      return of(false);
    }

    const token = this.authService.getToken();

    if (token === null) {
      return of(false);
    }

    return this.endpoint.useJwt(token).removeUser(user.id);
  }
}
