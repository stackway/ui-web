import { Injectable } from '@angular/core';

import { AuthService } from './auth-service';
import { UserManager } from './user-manager.service';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {
  constructor(
    private readonly authService: AuthService,
    private readonly userManager: UserManager
  ) {}

  public logout() {
    this.authService.logout();
    this.userManager.clearUser();
  }
}
