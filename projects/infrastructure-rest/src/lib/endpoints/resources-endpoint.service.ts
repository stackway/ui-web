import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { CrudService } from '../core/crud-service';

@Injectable({
  providedIn: 'root'
})
export class ResourcesEndpoint extends CrudService {
  constructor(client: HttpClient) {
    super(client, {
      resource: 'public/api/topics/:topicId/actual-resources/:resourceId'
    });
  }

  public archive(topicId: number, resourceId: number): Observable<boolean> {
    return this.client
      .post(
        this.builder
          .build('resource')
          .setNamedParameter('topicId', topicId)
          .setNamedParameter('resourceId', resourceId)
          .get(),
        '',
        { headers: this.headers }
      )
      .pipe(
        catchError((error: HttpErrorResponse) => of(false)),
        map(isSuccess => isSuccess !== false)
      );
  }
}
