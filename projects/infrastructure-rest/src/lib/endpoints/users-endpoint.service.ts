import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { CrudService } from '../core/crud-service';

import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersEndpoint extends CrudService {
  constructor(client: HttpClient) {
    super(client, {
      users: 'public/api/users',
      userById: '/public/api/users/:id',
      userByEmail: '/public/api/users/find'
    });
  }

  public getUserByEmail(email: string): Observable<User> {
    return this.client.get<User>(this.builder.build('userByEmail').get(), {
      headers: this.headers,
      params: { email }
    });
  }

  public getUserById(id: number): Observable<User> {
    return this.client.get<User>(
      this.builder
        .build('userById')
        .setNamedParameter('id', id)
        .get(),
      { headers: this.headers }
    );
  }

  public addUser(user: UserRegistrationModel): Observable<boolean> {
    return this.client
      .post(this.builder.build('users').get(), user, { headers: this.headers })
      .pipe(
        catchError((error: HttpErrorResponse) => of(false)),
        map(isSuccess => isSuccess !== false)
      );
  }

  public removeUser(id: number): Observable<boolean> {
    return this.client
      .delete(
        this.builder
          .build('userById')
          .setNamedParameter('id', id)
          .get(),
        { headers: this.headers }
      )
      .pipe(
        catchError((error: HttpErrorResponse) => of(false)),
        map(isSuccess => isSuccess !== false)
      );
  }
}

export abstract class UserRegistrationModel {
  public email: string;
  public password: string;
  public confirmPassword: string;
  public username: string;
}
