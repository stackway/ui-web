import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { CrudService } from '../core/crud-service';

@Injectable({
  providedIn: 'root'
})
export class AuthEndpoint extends CrudService {
  constructor(client: HttpClient) {
    super(client, {
      token: '/public/api/users/token',
      isValid: '/public/api/users/token/is-valid'
    });
  }

  public isValidToken(token: string): Observable<boolean> {
    return this.useJwt(token)
      .client.get<{ isValid: boolean }>(this.builder.build('isValid').get(), {
        headers: this.headers
      })
      .pipe(
        catchError((error: HttpErrorResponse) => of({ isValid: false })),
        map(response => (response.isValid ? true : false))
      );
  }

  public getToken(email: string, password: string): Observable<string> {
    return this.client.get<string>(this.builder.build('token').get(), {
      headers: this.headers,
      params: { Email: email, Password: password }
    });
  }
}
