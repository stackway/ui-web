import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { CrudService } from '../core/crud-service';

import { Tag } from '../models/tag';

@Injectable({
  providedIn: 'root'
})
export class TagsEndpoint extends CrudService {
  constructor(client: HttpClient) {
    super(client, {
      userTags: '/public/api/users/:userId/tags',
      userTag: '/public/api/users/:userId/tags/:tagId',
      tag: '/public/api/tags/:tagId'
    });
  }

  public getTags(userId: number): Observable<Tag[]> {
    return this.client
      .get<Tag[]>(
        this.builder
          .build('userTags')
          .setNamedParameter('userId', userId)
          .get(),
        { headers: this.headers }
      )
      .pipe(catchError((error: HttpErrorResponse) => []));
  }

  public getTag(userId: number, tagId: number): Observable<Tag | null> {
    return this.client
      .get<Tag>(
        this.builder
          .build('userTag')
          .setNamedParameter('userId', userId)
          .setNamedParameter('tagId', tagId)
          .get(),
        { headers: this.headers }
      )
      .pipe(catchError((error: HttpErrorResponse) => of(null)));
  }

  public updateTag(tag: Tag): Observable<boolean> {
    return this.client
      .put(
        this.builder
          .build('tag')
          .setNamedParameter('tagId', tag.id)
          .get(),
        tag,
        { headers: this.headers }
      )
      .pipe(
        catchError((error: HttpErrorResponse) => of(false)),
        map(isSuccess => isSuccess !== false)
      );
  }

  public removeTag(userId: number, tag: Tag): Observable<boolean> {
    return this.client
      .delete(
        this.builder
          .build('userTag')
          .setNamedParameter('userId', userId)
          .setNamedParameter('tagId', tag.id)
          .get(),
        { headers: this.headers }
      )
      .pipe(
        catchError((error: HttpErrorResponse) => of(false)),
        map(isSuccess => isSuccess !== false)
      );
  }

  public addTag(userId: number, tag: Tag): Observable<boolean> {
    return this.client
      .post(
        this.builder
          .build('userTags')
          .setNamedParameter('userId', userId)
          .get(),
        tag,
        { headers: this.headers }
      )
      .pipe(
        catchError((error: HttpErrorResponse) => of(false)),
        map(isSuccess => isSuccess !== false)
      );
  }
}
