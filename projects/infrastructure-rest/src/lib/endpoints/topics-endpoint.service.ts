import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { CrudService } from '../core/crud-service';

import { Topic } from '../models/topic';

@Injectable({
  providedIn: 'root'
})
export class TopicsEndpoint extends CrudService {
  constructor(client: HttpClient) {
    super(client, {
      topics: '/public/api/users/:userId/topics/',
      topic: '/public/api/users/:userId/topics/:topicId',
      forceUpdate: 'public/api/users/:userId/topics/force-update'
    });
  }

  public getTopics(userId: number): Observable<Topic[]> {
    return this.client
      .get<Topic[]>(
        this.builder
          .build('topics')
          .setNamedParameter('userId', userId)
          .get(),
        {
          headers: this.headers
        }
      )
      .pipe(catchError((error: HttpErrorResponse) => []));
  }

  public getTopic(userId: number, topicId: number): Observable<Topic | null> {
    return this.client
      .get<Topic>(
        this.builder
          .build('topic')
          .setNamedParameter('userId', userId)
          .setNamedParameter('topicId', topicId)
          .get(),
        {
          headers: this.headers
        }
      )
      .pipe(catchError((error: HttpErrorResponse) => of(null)));
  }

  public forceUpdateResources(userId: number): Observable<boolean> {
    return this.client
      .post(
        this.builder
          .build('forceUpdate')
          .setNamedParameter('userId', userId)
          .get(),
        '',
        { headers: this.headers }
      )
      .pipe(
        catchError((error: HttpErrorResponse) => of(false)),
        map(response => (response !== false))
      );
  }
}
