import { HttpClient, HttpHeaders } from '@angular/common/http';

import UrlBuilder from 'rest-api-url-builder';

import { environment } from '../../environments/environment';

const options = {
  baseURL: environment.backend
};

export abstract class CrudService {
  protected headers: HttpHeaders;
  protected builder: UrlBuilder;

  constructor(protected client: HttpClient, routes: { [route: string]: string }) {
    this.builder = new UrlBuilder(routes, options);
    this.headers = new HttpHeaders();

    this.setupHeaders();
  }

  private setupHeaders() {
    this.headers = this.headers.set('Content-Type', 'application/json');
  }

  public useJwt(token: string): this {
    this.headers = this.headers.set('Authorization', `Bearer ${token}`);

    return this;
  }
}
