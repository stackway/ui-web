export class Resource {
  id: number;
  displayLink: string;
  link: string;
  imageLink: string | null;
  title: string;
  description: string;
  publishedAt: Date;
  relevanceIndex: number;
  type: string;
  tags: string[];

  queryTags: string[];
}
