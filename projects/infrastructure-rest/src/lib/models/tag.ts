export class Tag {
  id?: number;
  name: string;
  views: number;
  recentlyUsed: Date;
  importance: number;
  isHidden?: boolean;
}
