import { Resource } from './resource';

export class Topic {
  id: number;
  name: string;
  description: string;
  type: string;
  actualResources: Array<Resource>;

  public useResources(resources: Array<Resource>): this {
    this.actualResources = resources;

    return this;
  }
}
