import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AuthEndpoint } from './endpoints/auth-endpoint.service';
import { UsersEndpoint } from './endpoints/users-endpoint.service';

@NgModule({
  declarations: [],
  imports: [HttpClientModule],
  exports: [],
  providers: [AuthEndpoint, UsersEndpoint]
})
export class RestApiLibModule {}
