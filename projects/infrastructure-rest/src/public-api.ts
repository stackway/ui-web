/*
 * Public API
 */

export { RestApiLibModule } from './lib/rest-api-lib.module';

export { User } from './lib/models/user';
export { Topic } from './lib/models/topic';
export { Tag } from './lib/models/tag';
export { Resource } from './lib/models/resource';

export { AuthEndpoint } from './lib/endpoints/auth-endpoint.service';
export { UsersEndpoint } from './lib/endpoints/users-endpoint.service';
export { TopicsEndpoint } from './lib/endpoints/topics-endpoint.service';
export { TagsEndpoint } from './lib/endpoints/tags-endpoint.service';
export { ResourcesEndpoint } from './lib/endpoints/resources-endpoint.service';
