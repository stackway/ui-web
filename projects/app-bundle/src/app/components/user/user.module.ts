import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgZorroAntdModule } from 'ng-zorro-antd';

import { UserRoutingModule } from './user-routing.module';

import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [UserRegistrationComponent],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    UserRoutingModule,
    NgZorroAntdModule
  ],
  exports: [UserRegistrationComponent]
})
export class UserModule {}
