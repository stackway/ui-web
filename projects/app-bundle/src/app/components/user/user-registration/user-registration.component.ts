import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { NzNotificationService } from 'ng-zorro-antd';

import { UsersEndpoint } from 'projects/infrastructure-rest/src/public-api';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.scss']
})
export class UserRegistrationComponent implements OnInit {
  public validateForm: FormGroup;

  public confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    }

    if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }

    return {};
  };

  constructor(
    private readonly endpoint: UsersEndpoint,
    private readonly router: Router,
    private readonly notification: NzNotificationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.validateForm = this.formBuilder.group({
      email: [null, [Validators.email, Validators.required]],
      password: [null, [Validators.required]],
      checkPassword: [null, [Validators.required, this.confirmationValidator]],
      nickname: [null, [Validators.required]],
      agree: [false]
    });
  }

  public signUp() {
    const user = {
      email: this.validateForm.get('email').value,
      password: this.validateForm.get('password').value,
      confirmPassword: this.validateForm.get('checkPassword').value,
      username: this.validateForm.get('nickname').value
    };

    this.endpoint.addUser(user).subscribe(isSuccess => {
      if (!isSuccess) {
        return this.notification.error(
          'Sign Up',
          'Check the form data or internet connection'
        );
      }

      this.router.navigate(['login']);
    });
  }

  public updateConfirmValidator() {
    Promise.resolve().then(() =>
      this.validateForm.controls.checkPassword.updateValueAndValidity()
    );
  }
}
