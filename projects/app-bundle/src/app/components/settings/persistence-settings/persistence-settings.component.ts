import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NzNotificationService } from 'ng-zorro-antd';

import { LogoutService, UserManager } from '@lib/auth';

@Component({
  selector: 'app-persistence-settings',
  templateUrl: './persistence-settings.component.html',
  styleUrls: ['./persistence-settings.component.scss']
})
export class PersistenceSettingsComponent implements OnInit {
  constructor(
    private readonly userManager: UserManager,
    private readonly logoutService: LogoutService,
    private readonly notificationService: NzNotificationService,
    private readonly router: Router
  ) {

  }

  ngOnInit() {
  }

  public removeUser() {
    this.userManager.removeUser().subscribe(isSuccessful => {
      if (!isSuccessful) {
        this.notificationService.error(
          'Remove user',
          'can\'t remove user, check connection or update the page'
        );

        return;
      }

      this.logoutService.logout();
      this.router.navigate(['login']);
    });
  }
}
