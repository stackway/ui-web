import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';

import { SettingsViewComponent } from './settings-view/settings-view.component';
import { PersistenceSettingsComponent } from './persistence-settings/persistence-settings.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';

@NgModule({
  declarations: [SettingsViewComponent, PersistenceSettingsComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    NgZorroAntdModule
  ],
  exports: [SettingsViewComponent, PersistenceSettingsComponent]
})
export class SettingsModule { }
