import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { NgZorroAntdModule } from 'ng-zorro-antd';
import { MaterialModule } from '../../meterial.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NewsRoutingModule } from './news-routing.module';

import { CombineTopicsPipe } from './pipes/combine-topics.pipe';
import { RelevantTagsPipe } from './pipes/relevant-tags.pipe';

import { NewsOverviewComponent } from './news-overview/news-overview.component';
import { TopicViewComponent } from './topic-view/topic-view.component';
import { ResourceViewComponent } from './resource-view/resource-view.component';

@NgModule({
  declarations: [
    NewsOverviewComponent,
    TopicViewComponent,
    ResourceViewComponent,
    CombineTopicsPipe,
    RelevantTagsPipe
  ],
  imports: [
    CommonModule,
    NewsRoutingModule,
    BrowserModule,
    ScrollingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    NgZorroAntdModule,
    FontAwesomeModule
  ],
  exports: [NewsOverviewComponent, TopicViewComponent, ResourceViewComponent]
})
export class NewsModule {}
