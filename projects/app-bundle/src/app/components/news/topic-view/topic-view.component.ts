import { Component, OnInit, Input } from '@angular/core';

import { Topic } from '@infrastructure/rest';

@Component({
  selector: 'app-topic-view',
  templateUrl: './topic-view.component.html',
  styleUrls: ['./topic-view.component.scss']
})
export class TopicViewComponent implements OnInit {
  @Input() public topic: Topic;

  constructor() {}

  ngOnInit() {}
}
