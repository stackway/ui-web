import { Component, OnInit, Input } from '@angular/core';

import { Resource, Topic } from '@infrastructure/rest';
import { ResourcesService } from '@lib/store';

@Component({
  selector: 'app-resource-view',
  templateUrl: './resource-view.component.html',
  styleUrls: ['./resource-view.component.scss']
})
export class ResourceViewComponent implements OnInit {
  @Input() public resource: Resource;
  @Input() public topic: Topic;

  constructor(private readonly resourceService: ResourcesService) {}

  ngOnInit() {}

  public archive() {
    this.resourceService.archive(this.topic, this.resource).subscribe();
  }

  public getTitle() {
    if (!this.resource || !this.resource.title) {
      return '';
    }

    return this.resource.title.replace(/(<([^>]+)>)/gi, '');
  }
}
