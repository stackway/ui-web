import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { NzNotificationService } from 'ng-zorro-antd';

import { Topic } from '@infrastructure/rest';

import { TopicsStore, TopicsService } from '@lib/store';

import { ExtendedResource } from '../models/extended-resource';
import { ExtendedResourcesService } from '../services/extended-resources.service';

@Component({
  selector: 'app-news-overview',
  templateUrl: './news-overview.component.html',
  styleUrls: ['./news-overview.component.scss']
})
export class NewsOverviewComponent implements OnInit, OnDestroy {
  public topics: Topic[] = [];

  public get currentResources(): number {
    return this.topics
      .map(t => t.actualResources.length)
      .reduce((acc, count) => acc + count, 0);
  }

  private subscriptions: {
    onTopicsPull: Subscription;
    onTopicPull: Subscription;
    onTopicUpdate: Subscription;
  };

  constructor(
    private readonly store: TopicsStore,
    private readonly service: TopicsService,
    private readonly extendedResourceService: ExtendedResourcesService,
    private readonly notification: NzNotificationService
  ) {}

  ngOnInit() {
    this.subscriptions = {
      onTopicsPull: this.store.onTopicsPull.subscribe(topics => {
        this.topics = topics;
      }),
      onTopicPull: this.store.onTopicPull.subscribe(topic => {
        if (topic === null) {
          return;
        }

        const topicIndex = this.topics.findIndex(value => value.id === topic.id);

        if (topicIndex === -1) {
          return this.topics.push(topic);
        }

        this.topics[topicIndex] = topic;
      }),
      onTopicUpdate: this.store.onTopicUpdate.subscribe(topic => {
        if (topic === null) {
          return;
        }

        const topicIndex = this.topics.findIndex(value => value.id === topic.id);

        this.topics[topicIndex] = topic;
      })
    };

    this.store.getTopics().subscribe(topics => {
      this.topics = topics;
    });
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.onTopicsPull.unsubscribe();
      this.subscriptions.onTopicPull.unsubscribe();
      this.subscriptions.onTopicUpdate.unsubscribe();
    }
  }

  public forceUpdate() {
    this.service.forceUpdate().subscribe(isSuccessful => {
      if (!isSuccessful) {
        this.notification.warning('Force update articles', 'request was canceled');
      }
    });
  }

  public hasAnyResources(): boolean {
    return this.topics.filter(t => t.actualResources.length !== 0).length !== 0;
  }
}
