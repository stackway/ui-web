import { Injectable } from '@angular/core';

import { Topic } from '@infrastructure/rest';

import { ExtendedResource } from '../models/extended-resource';

@Injectable({ providedIn: 'root' })
export class ExtendedResourcesService {
  transform(topics: Topic[]): ExtendedResource[] {
    if (!topics || topics.length === 0) {
      return [];
    }

    const resources = [];

    topics.forEach(topic =>
      resources.push(
        ...topic.actualResources.map(resource => new ExtendedResource(resource, topic))
      )
    );

    return resources;
  }
}
