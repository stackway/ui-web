import { Resource, Topic } from '@infrastructure/rest';

export class ExtendedResource extends Resource {
  public topic: Topic;

  public constructor(resource: Resource, topic: Topic) {
    super();

    this.id = resource.id;
    this.title = resource.title;
    this.description = resource.description;
    this.link = resource.link;

    this.displayLink = resource.displayLink;
    this.imageLink = resource.imageLink;
    this.publishedAt = resource.publishedAt;

    this.queryTags = resource.queryTags;
    this.tags = resource.tags;

    this.relevanceIndex = resource.relevanceIndex;

    this.type = resource.type;

    this.topic = topic;
  }
}
