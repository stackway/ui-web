import { Pipe, PipeTransform } from '@angular/core';

import { Topic } from '@infrastructure/rest';

import { ExtendedResource } from '../models/extended-resource';

@Pipe({ name: 'combineTopics' })
export class CombineTopicsPipe implements PipeTransform {
  transform(topics: Topic[]): ExtendedResource[] {
    if (!topics || topics.length === 0) {
      return [];
    }

    const resources: ExtendedResource[] = [];

    topics.forEach(topic =>
      resources.push(
        ...topic.actualResources.map(resource => new ExtendedResource(resource, topic))
      )
    );

    return resources.sort((one, another) => another.relevanceIndex - one.relevanceIndex);
  }
}
