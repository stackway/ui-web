import { Pipe, PipeTransform } from '@angular/core';

import { Resource } from '@infrastructure/rest';

@Pipe({ name: 'relevantTags' })
export class RelevantTagsPipe implements PipeTransform {
  transform(resource: Resource): string[] {
    const performedActualTags = resource.tags.map(t => t.toLowerCase());

    const splicedQueryTags = resource.queryTags
      .map(t => t.toLowerCase().split(' '))
      .reduce((acc, splicedTags) => acc.concat(splicedTags), []);

    return splicedQueryTags.filter(t => performedActualTags.some(a => a.includes(t)));
  }
}
