import { Component, OnInit, Input } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl
} from '@angular/forms';

import { NzNotificationService, NzModalRef } from 'ng-zorro-antd';

import { Tag } from '@infrastructure/rest';
import { TagsStore } from '@lib/store';

@Component({
  selector: 'app-add-tag-modal',
  templateUrl: './add-tag-modal.component.html',
  styleUrls: ['./add-tag-modal.component.scss']
})
export class AddTagModalComponent implements OnInit {
  @Input() public tags: Tag[];

  public validateForm: FormGroup;

  constructor(
    private readonly store: TagsStore,
    private readonly fb: FormBuilder,
    private readonly notificationService: NzNotificationService,
    private modal: NzModalRef
  ) {}

  public ngOnInit(): void {
    this.validateForm = this.fb.group({
      tagName: [
        null,
        [Validators.required, Validators.minLength(3), ExistTagValidator(this.tags)]
      ],
      importance: [null, [Validators.required, Validators.min(1), Validators.max(99)]]
    });
  }

  public submitForm(): void {
    this.store
      .addTag({
        name: this.validateForm.get('tagName').value,
        views: 0,
        recentlyUsed: new Date(),
        importance: this.validateForm.get('importance').value
      })
      .subscribe(tag => {
        if (tag === null) {
          this.notificationService.warning(
            "Can't add new tag",
            'Maybe tag is already exists'
          );

          return;
        }

        this.modal.close();
      });
  }
}

function ExistTagValidator(tags: Tag[]): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    return tags.filter(t => t.name === control.value).length > 0
      ? { exist: { value: 'tag is already exists' } }
      : null;
  };
}
