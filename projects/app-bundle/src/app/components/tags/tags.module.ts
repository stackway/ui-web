import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ScrollingModule } from '@angular/cdk/scrolling';

import { NgZorroAntdModule } from 'ng-zorro-antd';
import { MaterialModule } from '../../meterial.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { TagsRoutingModule } from './tags-routing.module';

import { TagsOverviewComponent } from './tags-overview/tags-overview.component';
import { AddTagModalComponent } from './add-tag-modal/add-tag-modal.component';
import { ViewTagModalComponent } from './view-tag-modal/view-tag-modal.component';

@NgModule({
  declarations: [TagsOverviewComponent, AddTagModalComponent, ViewTagModalComponent],
  imports: [
    CommonModule,
    TagsRoutingModule,
    BrowserModule,
    ScrollingModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    MaterialModule,
    FontAwesomeModule
  ],
  exports: [TagsOverviewComponent, AddTagModalComponent, ViewTagModalComponent],
  entryComponents: [AddTagModalComponent, ViewTagModalComponent]
})
export class TagsModule {}
