import { Component, OnInit, Input } from '@angular/core';

import { Tag } from '@infrastructure/rest';
import { TagsStore } from '@lib/store';
import { NzModalRef } from 'ng-zorro-antd';

@Component({
  selector: 'app-view-tag-modal',
  templateUrl: './view-tag-modal.component.html',
  styleUrls: ['./view-tag-modal.component.scss']
})
export class ViewTagModalComponent implements OnInit {
  @Input() public tag: Tag;

  constructor(private readonly store: TagsStore, private modal: NzModalRef) {}

  ngOnInit() {}

  public remove() {
    this.store.removeTag(this.tag).subscribe(tag => this.modal.close());
  }
}
