import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { Tag } from '@infrastructure/rest';
import { TagsStore } from '@lib/store';
import { NzModalService } from 'ng-zorro-antd';
import { AddTagModalComponent } from '../add-tag-modal/add-tag-modal.component';
import { ViewTagModalComponent } from '../view-tag-modal/view-tag-modal.component';

@Component({
  selector: 'app-tags-overview',
  templateUrl: './tags-overview.component.html',
  styleUrls: ['./tags-overview.component.scss']
})
export class TagsOverviewComponent implements OnInit, OnDestroy {
  public tags: Tag[] = [];

  private subscriptions: {
    onTagsPull: Subscription;
    onTagPull: Subscription;
    onAddTag: Subscription;
    onUpdateTag: Subscription;
    onRemoveTag: Subscription;
  };

  constructor(
    private readonly store: TagsStore,
    private readonly modalService: NzModalService
  ) {}

  ngOnInit() {
    this.subscriptions = {
      onTagsPull: this.store.onTagsPull.subscribe(tags => {
        this.tags = tags;
      }),
      onTagPull: this.store.onTagPull.subscribe(tag => {
        if (tag === null) {
          return;
        }

        const tagIndex = this.tags.findIndex(value => value.id === tag.id);

        if (tagIndex === -1) {
          return this.tags.push(tag);
        }

        this.tags[tagIndex] = tag;
      }),
      onAddTag: this.store.onAddTag.subscribe(tag => {
        if (tag === null) {
          return;
        }

        this.tags.push(tag);
      }),
      onUpdateTag: this.store.onUpdateTag.subscribe(tag => {
        if (tag === null) {
          return;
        }

        const tagIndex = this.tags.findIndex(value => value.id === tag.id);
        this.tags[tagIndex] = tag;
      }),
      onRemoveTag: this.store.onRemoveTag.subscribe(tag => {
        if (tag === null) {
          return;
        }

        this.tags = this.tags.filter(t => t.id !== tag.id);
      })
    };

    this.store.getTags().subscribe(tags => {
      this.tags = tags;
    });
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.onTagsPull.unsubscribe();
      this.subscriptions.onTagPull.unsubscribe();
      this.subscriptions.onUpdateTag.unsubscribe();
      this.subscriptions.onAddTag.unsubscribe();
      this.subscriptions.onRemoveTag.unsubscribe();
    }
  }

  public showAddTagModal() {
    const modal = this.modalService.create({
      nzTitle: 'Add new tag',
      nzContent: AddTagModalComponent,
      nzComponentParams: {
        tags: this.tags
      },
      nzFooter: [
        {
          label: 'Close',
          shape: 'default',
          onClick: () => modal.destroy()
        }
      ]
    });
  }

  public showViewTagModal(tag: Tag) {
    const modal = this.modalService.create({
      nzTitle: 'Tag overview',
      nzContent: ViewTagModalComponent,
      nzComponentParams: { tag },
      nzFooter: [
        {
          label: 'Close',
          shape: 'default',
          onClick: () => modal.destroy()
        }
      ]
    });
  }
}
