import { NgModule } from '@angular/core';

import { NavigationModule } from './navigation/navigation.module';
import { NewsModule } from './news/news.module';
import { UserModule } from './user/user.module';
import { TagsModule } from './tags/tags.module';
import { SettingsModule } from './settings/settings.module';

@NgModule({
  exports: [NavigationModule, NewsModule, UserModule, TagsModule, SettingsModule]
})
export class ComponentsModule {}
