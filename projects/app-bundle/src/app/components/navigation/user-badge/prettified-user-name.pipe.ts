import { Pipe, PipeTransform } from '@angular/core';

import { PersistedUserContract } from '@lib/auth';

@Pipe({
  name: 'prettifiedUserName'
})
export class PrettifiedUserNamePipe implements PipeTransform {
  transform(user: PersistedUserContract): string {
    if (!user || !user.email) {
      return 'Mysterious Stranger';
    }

    return user.username ? user.username : user.email;
  }
}
