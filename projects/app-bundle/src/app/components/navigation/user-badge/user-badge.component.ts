import { Component, OnInit, Input } from '@angular/core';

import { PersistedUserContract } from '@lib/auth';

@Component({
  selector: 'app-user-badge',
  templateUrl: './user-badge.component.html',
  styleUrls: ['./user-badge.component.scss']
})
export class UserBadgeComponent implements OnInit {
  @Input() public user: PersistedUserContract | null;

  constructor() {}

  ngOnInit() {}
}
