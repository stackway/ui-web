import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NgZorroAntdModule } from 'ng-zorro-antd';
import { MaterialModule } from '../../meterial.module';

import { UserBadgeComponent } from './user-badge/user-badge.component';
import { PrettifiedUserNamePipe } from './user-badge/prettified-user-name.pipe';

@NgModule({
  declarations: [UserBadgeComponent, PrettifiedUserNamePipe],
  imports: [CommonModule, MaterialModule, NgZorroAntdModule, FontAwesomeModule],
  exports: [UserBadgeComponent]
})
export class NavigationModule {}
