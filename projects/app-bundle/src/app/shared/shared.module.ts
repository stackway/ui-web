import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthLibModule } from '@lib/auth';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  exports: [AuthLibModule]
})
export class SharedModule {}
