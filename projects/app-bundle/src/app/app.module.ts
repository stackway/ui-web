import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';

import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faNewspaper as farNewspaper } from '@fortawesome/free-regular-svg-icons';
import {
  faSignOutAlt,
  faSignInAlt,
  faUserPlus,
  faIdBadge,
  faCheckCircle,
  faPlus,
  faSyncAlt
} from '@fortawesome/free-solid-svg-icons';
import { faReadme } from '@fortawesome/free-brands-svg-icons';

import { MaterialModule } from './meterial.module';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './routing/app-routing.module';

import { NavigationComponent } from './layout/navigation/navigation.component';
import { ComponentsModule } from './components/components.module';

registerLocaleData(en);

@NgModule({
  declarations: [AppComponent, NavigationComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    NgZorroAntdModule,
    FontAwesomeModule,

    AppRoutingModule,

    SharedModule,
    ComponentsModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    library.add(
      farNewspaper,
      faSignInAlt,
      faSignOutAlt,
      faUserPlus,
      faIdBadge,
      faReadme,
      faCheckCircle,
      faPlus,
      faSyncAlt
    );
  }
}
