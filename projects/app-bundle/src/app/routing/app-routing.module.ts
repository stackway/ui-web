import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardOptions, LoginComponent, LoginRouting, AuthGuard } from '@lib/auth';

import { UserRegistrationComponent } from '../components/user/user-registration/user-registration.component';
import { NewsOverviewComponent } from '../components/news/news-overview/news-overview.component';
import { TagsOverviewComponent } from '../components/tags/tags-overview/tags-overview.component';
import { SettingsViewComponent } from '../components/settings/settings-view/settings-view.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'sign-up', component: UserRegistrationComponent },
  { path: 'news', component: NewsOverviewComponent, canActivate: [AuthGuard] },
  { path: 'tags', component: TagsOverviewComponent, canActivate: [AuthGuard] },
  { path: 'settings', component: SettingsViewComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: AuthGuardOptions,
      useValue: {
        routes: {
          invalid: { use: true, link: 'login' },
          success: { use: true, link: 'news' }
        }
      }
    },
    { provide: LoginRouting, useValue: { success: 'news' } }
  ]
})
export class AppRoutingModule {
}
