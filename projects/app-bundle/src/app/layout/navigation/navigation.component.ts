import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';

import {
  UserManager,
  AuthService,
  PersistedUserContract,
  LogoutService
} from '@lib/auth';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {
  @Input() public isCollapsed;

  public isAuthenticated: boolean;
  public user: PersistedUserContract | null;

  private subscriptions: {
    onAuthentication: Subscription;
    onPersistUser: Subscription;
  } | null;

  constructor(
    private readonly authService: AuthService,
    private readonly userManager: UserManager,
    private readonly logoutService: LogoutService,
    private readonly router: Router
  ) {
    this.subscriptions = {
      onAuthentication: this.authService.onAuthentication.subscribe(
        state => (this.isAuthenticated = state)
      ),
      onPersistUser: this.userManager.onPersistedUserChange.subscribe(
        user => (this.user = user)
      )
    };
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.onAuthentication.unsubscribe();
      this.subscriptions.onPersistUser.unsubscribe();
    }
  }

  public logout() {
    this.logoutService.logout();
    this.router.navigate(['login']);
  }
}
